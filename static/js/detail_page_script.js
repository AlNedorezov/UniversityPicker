JS_DETAIL = {


    formUniversitiy: function (json) {

        var length = json.length;

        $html = '';

        json.forEach(function (item, i, arr) {

            var fields = item.fields;
            var name = item.full_name;

            var region = 'Republic of Tatarstan';
            var city = 'Innopolis';
            var desc = item.description;

            var salary = 75000;
            var crime_level = 'Below Average';

            $html += '';

        });


        return $html;
    }
}


$(document).ready(function () {

    var json = formJson();

    ajaxQuery(json);


    function ajaxQuery(json) {

        $.ajax({
            url: "/universities/details/",
            data: json,
            type: "POST",
            dataType: "json"
        })
            .done(function (json) {
                console.log("json = " + json);
                // var html = JS_DETAIL.formUniversitiy(json);

                // $('#test_par').html(json);

                json.forEach(function (item, i, arr) {

                    var fields = item.fields;
                    var name = item.full_name;
                    var website = item.website;
                    var description = item.description;

                    var region = item.region;
                    var city = item.city;
                    var desc = item.description;

                    var salary = item.salary;
                    var crime_level = item.crime_level;

                    var special = item.specialties;




                    //console.log(special);


                    $('#p2').html(name);
                    $('#p_value_sal').html(salary);
                    $('#p_value_crime_level').html(crime_level);
                    $('#univ_href').html(website);
                    $('#content_description').html(description);
                    $('#location_name').html(region + ', ' + city);

                    formSpecArray(special);


                    $html += '';

                });


                // $('#table_universities tbody:last').html('').append(html);
            })
            .fail(function () {
            })
            .always(function () {
            });

    }

    function formSpecArray(json) {

        var speciality_item = {};

        var defaultData = [];


        var node = {};

        $.each(JSON.parse(json), function (arrayID, group) {
            var spec_name = group.name;
            var spec_positions_description = group.positions_description;
            var spec_desc = group.description;
            var jobs_json = group.jobs;


            var nodes_array = [];
            node = {
                text: JQUERY4U.formNode(spec_desc, spec_positions_description, jobs_json),
                href: '#node_' + spec_name,
                tags: ['1'],

            };

            nodes_array.push(node);

            speciality_item = {

                text: spec_name,
                href: '#' + spec_name,
                tags: ['0'],
                nodes: nodes_array

            };

            defaultData.push(speciality_item);

        });

        loadTree(defaultData);
    }

    function formJson() {

        var json = '';


        /* json = json.slice(0, -1);
         var json = '{' + json + '}';*/

        var id = $('#p2').attr("title");

        json = '{"id": "' + id + '"}';


        return json;
    }


    JQUERY4U = {
        formNode: function (spec_desc, spec_desc_type, jobs_json) {

            $html = '<div id ="spec_div">';
            $html += ' <div> <p id="node_header">Описание специальности:</p> <p id="node_desc"> ' + spec_desc + '  </p> </div>';
            $html += ' <div> <p id="node_header">Описание возможных типов деятельности по окончанию специальности:</p> <p id="node_desc"> ' + spec_desc_type + '  </p> </div>';

            $html +='<div> <p id="node_header">Возможные вакансии и уровень заработной платы по окончанию специальности</p> <p id="node_desc"> <ul>';
            $.each(JSON.parse(jobs_json), function (arrayID1, job) {

                            var job_salary = job.salary;
                            var job_city = job.city;
                            var job_name = job.name;

                $html += '<li class="spec_li">' + job_name + ' , ' +  job_city  +  ' , ' + job_salary + ' руб' +   '</li>';

                        });

            $html += ' </ul> </p> </div> </div>';

            return $html;
        }
    }
    function loadTree(defaultData) {


        $('#treeview1').treeview({
            data: defaultData
        });

        $('#treeview2').treeview({
            levels: 1,
            data: defaultData
        });

        $('#treeview3').treeview({
            levels: 99,
            data: defaultData
        });

        $('#treeview4').treeview({

            color: "#428bca",
            data: defaultData
        });

        $('#treeview5').treeview({
            color: "#428bca",
            expandIcon: 'glyphicon glyphicon-chevron-right',
            collapseIcon: 'glyphicon glyphicon-chevron-down',
            nodeIcon: 'glyphicon glyphicon-bookmark',
            data: defaultData
        });

        $('#treeview6').treeview({
            color: "#428bca",
            expandIcon: "glyphicon glyphicon-stop",
            collapseIcon: "glyphicon glyphicon-unchecked",
            nodeIcon: "glyphicon glyphicon-user",
            showTags: true,
            data: defaultData
        });

        $('#treeview7').treeview({
            color: "#428bca",
            showBorder: false,
            data: defaultData
        });

        $('#treeview8').treeview({
            expandIcon: "glyphicon glyphicon-stop",
            collapseIcon: "glyphicon glyphicon-unchecked",
            nodeIcon: "glyphicon glyphicon-user",
            color: "yellow",
            backColor: "purple",
            onhoverColor: "orange",
            borderColor: "red",
            showBorder: false,
            showTags: true,
            highlightSelected: true,
            selectedColor: "yellow",
            selectedBackColor: "darkorange",
            data: defaultData
        });

        $('#treeview9').treeview({
            expandIcon: "glyphicon glyphicon-stop",
            collapseIcon: "glyphicon glyphicon-unchecked",
            nodeIcon: "glyphicon glyphicon-user",
            color: "yellow",
            backColor: "purple",
            onhoverColor: "orange",
            borderColor: "red",
            showBorder: false,
            showTags: true,
            highlightSelected: true,
            selectedColor: "yellow",
            selectedBackColor: "darkorange",
            data: alternateData
        });

        $('#treeview10').treeview({
            color: "#428bca",
            enableLinks: true,
            data: defaultData
        });


        var $searchableTree = $('#treeview-searchable').treeview({
            data: defaultData,
        });

        var search = function (e) {
            var pattern = $('#input-search').val();
            var options = {
                ignoreCase: $('#chk-ignore-case').is(':checked'),
                exactMatch: $('#chk-exact-match').is(':checked'),
                revealResults: $('#chk-reveal-results').is(':checked')
            };
            var results = $searchableTree.treeview('search', [pattern, options]);

            var output = '<p>' + results.length + ' matches found</p>';
            $.each(results, function (index, result) {
                output += '<p>- ' + result.text + '</p>';
            });
            $('#search-output').html(output);
        }

        $('#btn-search').on('click', search);
        $('#input-search').on('keyup', search);

        $('#btn-clear-search').on('click', function (e) {
            $searchableTree.treeview('clearSearch');
            $('#input-search').val('');
            $('#search-output').html('');
        });


        var initSelectableTree = function () {
            return $('#treeview-selectable').treeview({
                data: defaultData,
                multiSelect: $('#chk-select-multi').is(':checked'),
                onNodeSelected: function (event, node) {
                    $('#selectable-output').prepend('<p>' + node.text + ' was selected</p>');
                },
                onNodeUnselected: function (event, node) {
                    $('#selectable-output').prepend('<p>' + node.text + ' was unselected</p>');
                }
            });
        };
        var $selectableTree = initSelectableTree();

        var findSelectableNodes = function () {
            return $selectableTree.treeview('search', [$('#input-select-node').val(), {
                ignoreCase: false,
                exactMatch: false
            }]);
        };
        var selectableNodes = findSelectableNodes();

        $('#chk-select-multi:checkbox').on('change', function () {
            console.log('multi-select change');
            $selectableTree = initSelectableTree();
            selectableNodes = findSelectableNodes();
        });

        // Select/unselect/toggle nodes
        $('#input-select-node').on('keyup', function (e) {
            selectableNodes = findSelectableNodes();
            $('.select-node').prop('disabled', !(selectableNodes.length >= 1));
        });

        $('#btn-select-node.select-node').on('click', function (e) {
            $selectableTree.treeview('selectNode', [selectableNodes, {silent: $('#chk-select-silent').is(':checked')}]);
        });

        $('#btn-unselect-node.select-node').on('click', function (e) {
            $selectableTree.treeview('unselectNode', [selectableNodes, {silent: $('#chk-select-silent').is(':checked')}]);
        });

        $('#btn-toggle-selected.select-node').on('click', function (e) {
            $selectableTree.treeview('toggleNodeSelected', [selectableNodes, {silent: $('#chk-select-silent').is(':checked')}]);
        });


        var $expandibleTree = $('#treeview-expandible').treeview({
            data: defaultData,
            onNodeCollapsed: function (event, node) {
                $('#expandible-output').prepend('<p>' + node.text + ' was collapsed</p>');
            },
            onNodeExpanded: function (event, node) {
                $('#expandible-output').prepend('<p>' + node.text + ' was expanded</p>');
            }
        });

        var findExpandibleNodess = function () {
            return $expandibleTree.treeview('search', [$('#input-expand-node').val(), {
                ignoreCase: false,
                exactMatch: false
            }]);
        };
        var expandibleNodes = findExpandibleNodess();

        // Expand/collapse/toggle nodes
        $('#input-expand-node').on('keyup', function (e) {
            expandibleNodes = findExpandibleNodess();
            $('.expand-node').prop('disabled', !(expandibleNodes.length >= 1));
        });

        $('#btn-expand-node.expand-node').on('click', function (e) {
            var levels = $('#select-expand-node-levels').val();
            $expandibleTree.treeview('expandNode', [expandibleNodes, {
                levels: levels,
                silent: $('#chk-expand-silent').is(':checked')
            }]);
        });

        $('#btn-collapse-node.expand-node').on('click', function (e) {
            $expandibleTree.treeview('collapseNode', [expandibleNodes, {silent: $('#chk-expand-silent').is(':checked')}]);
        });

        $('#btn-toggle-expanded.expand-node').on('click', function (e) {
            $expandibleTree.treeview('toggleNodeExpanded', [expandibleNodes, {silent: $('#chk-expand-silent').is(':checked')}]);
        });

        // Expand/collapse all
        $('#btn-expand-all').on('click', function (e) {
            var levels = $('#select-expand-all-levels').val();
            $expandibleTree.treeview('expandAll', {levels: levels, silent: $('#chk-expand-silent').is(':checked')});
        });

        $('#btn-collapse-all').on('click', function (e) {
            $expandibleTree.treeview('collapseAll', {silent: $('#chk-expand-silent').is(':checked')});
        });


        var $checkableTree = $('#treeview-checkable').treeview({
            data: defaultData,
            showIcon: false,
            showCheckbox: true,
            onNodeChecked: function (event, node) {
                $('#checkable-output').prepend('<p>' + node.text + ' was checked</p>');
            },
            onNodeUnchecked: function (event, node) {
                $('#checkable-output').prepend('<p>' + node.text + ' was unchecked</p>');
            }
        });

        var findCheckableNodess = function () {
            return $checkableTree.treeview('search', [$('#input-check-node').val(), {
                ignoreCase: false,
                exactMatch: false
            }]);
        };
        var checkableNodes = findCheckableNodess();

        // Check/uncheck/toggle nodes
        $('#input-check-node').on('keyup', function (e) {
            checkableNodes = findCheckableNodess();
            $('.check-node').prop('disabled', !(checkableNodes.length >= 1));
        });

        $('#btn-check-node.check-node').on('click', function (e) {
            $checkableTree.treeview('checkNode', [checkableNodes, {silent: $('#chk-check-silent').is(':checked')}]);
        });

        $('#btn-uncheck-node.check-node').on('click', function (e) {
            $checkableTree.treeview('uncheckNode', [checkableNodes, {silent: $('#chk-check-silent').is(':checked')}]);
        });

        $('#btn-toggle-checked.check-node').on('click', function (e) {
            $checkableTree.treeview('toggleNodeChecked', [checkableNodes, {silent: $('#chk-check-silent').is(':checked')}]);
        });

        // Check/uncheck all
        $('#btn-check-all').on('click', function (e) {
            $checkableTree.treeview('checkAll', {silent: $('#chk-check-silent').is(':checked')});
        });

        $('#btn-uncheck-all').on('click', function (e) {
            $checkableTree.treeview('uncheckAll', {silent: $('#chk-check-silent').is(':checked')});
        });


        var $disabledTree = $('#treeview-disabled').treeview({
            data: defaultData,
            onNodeDisabled: function (event, node) {
                $('#disabled-output').prepend('<p>' + node.text + ' was disabled</p>');
            },
            onNodeEnabled: function (event, node) {
                $('#disabled-output').prepend('<p>' + node.text + ' was enabled</p>');
            },
            onNodeCollapsed: function (event, node) {
                $('#disabled-output').prepend('<p>' + node.text + ' was collapsed</p>');
            },
            onNodeUnchecked: function (event, node) {
                $('#disabled-output').prepend('<p>' + node.text + ' was unchecked</p>');
            },
            onNodeUnselected: function (event, node) {
                $('#disabled-output').prepend('<p>' + node.text + ' was unselected</p>');
            }
        });

        var findDisabledNodes = function () {
            return $disabledTree.treeview('search', [$('#input-disable-node').val(), {
                ignoreCase: false,
                exactMatch: false
            }]);
        };
        var disabledNodes = findDisabledNodes();

        // Expand/collapse/toggle nodes
        $('#input-disable-node').on('keyup', function (e) {
            disabledNodes = findDisabledNodes();
            $('.disable-node').prop('disabled', !(disabledNodes.length >= 1));
        });

        $('#btn-disable-node.disable-node').on('click', function (e) {
            $disabledTree.treeview('disableNode', [disabledNodes, {silent: $('#chk-disable-silent').is(':checked')}]);
        });

        $('#btn-enable-node.disable-node').on('click', function (e) {
            $disabledTree.treeview('enableNode', [disabledNodes, {silent: $('#chk-disable-silent').is(':checked')}]);
        });

        $('#btn-toggle-disabled.disable-node').on('click', function (e) {
            $disabledTree.treeview('toggleNodeDisabled', [disabledNodes, {silent: $('#chk-disable-silent').is(':checked')}]);
        });

        // Expand/collapse all
        $('#btn-disable-all').on('click', function (e) {
            $disabledTree.treeview('disableAll', {silent: $('#chk-disable-silent').is(':checked')});
        });

        $('#btn-enable-all').on('click', function (e) {
            $disabledTree.treeview('enableAll', {silent: $('#chk-disable-silent').is(':checked')});
        });


        var $tree = $('#treeview12').treeview({
            data: json
        });
    }

});