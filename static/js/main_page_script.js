JQUERY4U = {

    formSubjects: function () {

        $html = '<div id="subjects_div" class="col-md-4"> ' +
            '<div id="subitem_div" class="col-md-2"> ' +
            '<select> ' +
            '<option value="information_technology">Information Technology</option> ' +
            '<option value="mathematica">Mathematica</option> ' +
            '<option value="english">English</option> ' +
            '<option value="russian_history">Russian History</option> ' +
            '<option value="geography">Geography</option> ' +
            '<option value="chemistry">Chemistry</option> ' +
            '<option value="physics">Physics</option> ' +
            '<option value="social_science">Social Science</option> ' +
            '<option value="literature">Literature</option> ' +
            '</select> </div> <div id="subitem_div" class="col-md-2"> ' +
            '<input type="number" class="input_subjects" id="subject_1"/> ' +
            '</div> ' +
            '</div>';

        return $html;
    },

    formRegions: function (json) {

        var length = json.length;

        $html = '';

        $html += ' <option value="choose_region">Region of the university</option>'

        json.forEach(function (item, i, arr) {

            var name = item.fields.name;

            $html += '<option value="' + name + '">' + name + '</option>';

        });

        return $html;
    },

    formCities: function (json) {

        var length = json.length;

        $html = '';

        $html += ' <option value="city">City of the university</option>'

        json.forEach(function (item, i, arr) {

            var name = item.fields.name;

            $html += '<option value="' + name + '">' + name + '</option>';

        });

        return $html;
    },

    formUniversities: function (json) {

        var length = json.length;

        console.log('json = ' + json);

        $html = '';

        $html += ' <tr> <td><p id="p_border">Name, description and locaion of the university</p></td> <td class="center_td"><p id="p_border">Average salary in region, RUB</p></td> <td class="center_td"><p id="p_border">Crime level in region</p></td> </tr>';

        json.forEach(function (item, i, arr) {
            console.log(item);
            //var name = fields.full_name;
            var name = item.name;

            var id = item.id;

            var region = item.region;
            var city = item.city;
            var desc = item.description;

            var salary = item.salary;
            var crime_level = item.crime_level;

            $html += '<tr> ' +
                '<td id="td_name"> ' +
                '<div class="row"> ' +
                '<div class="col-md-10"> ' +
                '<div id="locations_name"> ' +
                '<div id="block-locations"> ' +
                '<a class="univ_href" target="_blank" href="http://127.0.0.1:8000/universities/' + id + '/">' +
                '<p id="university_name"> ' + name + '</p>' +
                '</a> ' +
                '</div> ' +
                '<div id="block-locations"> ' +
                '<img style="height: 9pt" src="/static/img/location.png"> ' +
                '</div> ' +
                '<div id="block-locations"> ' +
                '<p id="location_name">' + region + ',</p> ' +
                '</div> ' +
                '<div id="block-locations"> ' +
                '<p id="location_name"> ' + city + '</p> ' +
                '</div> ' +
                '</div> ' +
                '<div id="locations_content"> ' +
                '<p1 id="location_text"> ' + desc + ' </p1> ' +
                '</div> ' +
                '</div> ' +
                '</div> ' +
                '</td> ' +
                '<td id="td_price">' +
                '<p id="p_td">' + salary + '</p>' +
                '</td> ' +
                '<td id="td_average">' +
                '<p id="p_td">' + crime_level + '</p>' +
                '</td> ' +
                '</tr>';
        });
        return $html;
    }
}


$(document).ready(function () {

    ajaxQueryRegionsList('{}');

    ajaxQueryCitiesList('{}');

    $('#btn_add_exam').click(function () {


        var html = JQUERY4U.formSubjects();

        $('#subjects_btn_div').after(html);
    });


    $("#selector_region").change(function () {
        var json = formJsonUniversities();


        //$("#test_pppp").html(jsonRegion);

        ajaxQueryUniversitiesList(json);

        var jsonRegion = formJsonCityByRegion();

        if (jsonRegion.length > 1) {

            ajaxQueryCitiesByRegion(jsonRegion);
        }
        else {

            ajaxQueryCitiesList('{}');
        }



    });

    $("#selector_city").change(function () {

        var json = formJsonUniversities();

        //   $("#test_pppp").html(json);
        //blabla

        ajaxQueryUniversitiesList(json);

        var jsonCity = formJsonRegionsByCitiy();

        if (jsonCity.length > 1) {

            ajaxQueryRegionsByCity(jsonCity);
        }
        else {

            ajaxQueryRegionsList('{}');
        }
    });

    $("#selector_crime_level").change(function () {

        var json = formJsonUniversities();

        //   $("#test_pppp").html(json);

        ajaxQueryUniversitiesList(json);
    });


    $("#cb_dominatory").change(function () {

        $("#cb_dominatory").val("was_checked");

        var json = formJsonUniversities();

        //    $("#test_pppp").html(json);

        ajaxQueryUniversitiesList(json);

    });

    function ajaxQueryUniversitiesList(json) {

        $.ajax({
            url: "/universities/list/",
            data: json,
            type: "POST",
            dataType: "json"
        })
            .done(function (json) {
                console.log("done");
                var html = JQUERY4U.formUniversities(json);


                $('#table_universities tbody:last').html('').append(html);
            })
            .fail(function () {
                console.log("fail");
            })
            .always(function () {
                console.log("always");
            });

    }

    function ajaxQueryRegionsList(json) {

        $.ajax({
            url: "/universities/allregions/",
            data: json,
            type: "POST",
            dataType: "json"
        })
            .done(function (json) {
                console.log("done");
                var html = JQUERY4U.formRegions(json);

                $('#selector_region').html('').append(html);
            })
            .fail(function () {
                console.log("fail");
            })
            .always(function () {
                console.log("always");
            });

    }


    function ajaxQueryCitiesList(json) {

        $.ajax({
            url: "/universities/allcities/",
            data: json,
            type: "POST",
            dataType: "json"
        })
            .done(function (json) {
                console.log("done");
                var html = JQUERY4U.formCities(json);

                $('#selector_city').html('').append(html);
            })
            .fail(function () {
                console.log("fail");
            })
            .always(function () {
                console.log("always");
            });

    }

    function ajaxQueryCitiesByRegion(json) {

        $.ajax({
            url: "/universities/citiesbyregion/",
            data: json,
            type: "POST",
            dataType: "json"
        })
            .done(function (json) {
                console.log("done");
                var html = JQUERY4U.formCities(json);

                $('#selector_city').html('').append(html);
            })
            .fail(function () {
                console.log("fail");
            })
            .always(function () {
                console.log("always");
            });

    }

    function ajaxQueryRegionsByCity(json) {

        $.ajax({
            url: "/universities/regionbycity/",
            data: json,
            type: "POST",
            dataType: "json"
        })
            .done(function (json) {
                console.log("done");
                var html = JQUERY4U.formRegions(json);

                $('#selector_region').html('').append(html);
            })
            .fail(function () {
                console.log("fail");
            })
            .always(function () {
                console.log("always");
            });

    }

    function formJsonCityByRegion() {

        var json = '';

        var region_name = $("#selector_region :selected").val();

        var position_region = $("#selector_region :selected").index();

        if (position_region == 0) {

            json = '';
        }
        else {
            json += '"region":' + '"' + region_name + '"' + ',';

        }

        if (json.length > 1) {

            json = json.slice(0, -1);
            var json = '{' + json + '}';
        }

        return json;

    }

    function formJsonRegionsByCitiy() {

        var json = '';

        var city_name = $("#selector_city :selected").val();
        var posiion_city = $("#selector_city :selected").index();

        if (posiion_city > 0) {

            json += '"city":' + '"' + city_name + '"' + ',';
        }
        else {
            json = '';
        }

        if (json.length > 1) {

            json = json.slice(0, -1);
            var json = '{' + json + '}';
        }

        return json;

    }


    function formJsonUniversities() {

        var json = '';
        //city
        var city_name = $("#selector_city :selected").val();
        var posiion_city = $("#selector_city :selected").index();

        if (posiion_city > 0) {

            json += '"city":' + '"' + city_name + '"' + ',';
        }
        else {
            city_name = '';
        }

        //---------------------------------------

        //---------------------------------------
        //region
        var region_name = $("#selector_region :selected").val();

        var position_region = $("#selector_region :selected").index();

        if (position_region == 0) {

            region_name = '';
        }
        else {
            json += '"region":' + '"' + region_name + '"' + ',';

        }

        //I am interested in living in dormitory

        var cb_val = $("#cb_dominatory").val();

        if (cb_val.length > 1) {

            if ($('#cb_dominatory').is(':checked')) {

                json += '"accomodations":' + true + ',';
            }
            else {

                json += '"accomodations":' + false + ',';
            }
        }

        //crime level

        var crime_level_value = $("#selector_crime_level :selected").val();

        var crime_level_index = $("#selector_crime_level :selected").index();

        if (crime_level_index > 0) {

            if (json.indexOf('city') > 0) {

                json += '"crime_level":' + '"' + crime_level_value + '"' + ',';
            }
            else if (json.indexOf('region') > 0) {

                json += '"crime_level":' + '"' + crime_level_value + '"' + ',';
            }
	    //else {
            //    json += '"crime_level":' + '"' + crime_level_value + '"' + ',';
	    //}
        }

        json = json.slice(0, -1);
        var json = '{' + json + '}';

        return json;
    }

});
