# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from universities.models import University, Region, City, Job, Department, Accommodations
from universities.models import SpecialtyDTO, UniversitiesSpecialty, SpecialtyJob
from universities.serializers import UniverSerializer, UniverDetailSerializer

from django.core import serializers

import universities.management.commands.fetch_specialties
import universities.management.commands.map_jobs_to_specialties
import universities.management.commands.populate_db
import threading
#import logging
import json
import sys

reload(sys)
sys.setdefaultencoding('utf8')
#logger = logging.getLogger('Views')


def index(request):
    return render_to_response('landing/index.html')


def detail(request):
    return render_to_response('landing/detail.html')


def university(request, university_id):
    return render_to_response('landing/detail.html', {'univ_id': university_id})


@csrf_exempt
def university_details(request):
    if request.method == 'POST':
      #  logger.debug('TEST: %s' % str(request.body))
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)

    univer_list = []
    if 'id' in body:
        university_obj = University.objects.filter(id=body['id'])
        university = university_obj[0]
        city_obj = City.objects.filter(name=university.city)[0]
        region_obj = Region.objects.filter(cities=city_obj)[0]
        u_dto = University.to_dto(university, region_obj.name)

        # get salary
        salary = 0
        jobs = list(Job.objects.filter(city=city_obj).order_by('-salary'))
        if len(jobs) > 0:
            salary = str(jobs[0].salary)

        # get specialties
        univer_to_specialties = list(UniversitiesSpecialty.objects.filter(university=university))
        specialties = []
        if len(univer_to_specialties) > 0:
            for u_to_s in univer_to_specialties:
                specialty = SpecialtyDTO()
                jobs = set()
                for job_to_speciality in list(SpecialtyJob.objects.filter(specialty=u_to_s.specialty)):
                    jobs.add(job_to_speciality.job)

                specialty.name = u_to_s.specialty.name
                specialty.description = u_to_s.specialty.description
                specialty.positions_description = u_to_s.specialty.possible_positions_prose
                specialty.jobs = list(jobs)

                if len(specialty.jobs) > 0:
                    for job in specialty.jobs:
                        print(str(job.name) + ' ' + str(job.salary) + ' ' + str(job.city))

                specialties.append(SpecialtyDTO.to_dto(specialty))

        u_dto.specialties = specialties
        u_dto.salary = salary
        u_dto.crime_level = city_obj.city_crime_level
        univer_list.append(u_dto)

    serializer = UniverDetailSerializer()
    data = serializer.serialize(univer_list)

    return HttpResponse(data, content_type='application/json; charset=UTF-8')


# todo refactoring.
@csrf_exempt
def universities_list(request):
    if request.method == 'POST':
       # logging.debug(str(request.body))
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)

        univer_list = set()
        city_obj = {}
        salary = {}
        salaries = {}
        crime_levels = {}
        region_obj = None
        crime_level = ''
        accommodations = ''

        # filter by crime level
        if ('crime_level' in body) and ('city' not in body) and ('region' not in body):
            # if 'crime_level' in body:
            print 'in crime level filter'
            cities = City.objects.filter(city_crime_level=body['crime_level'])
            print 'cities length:' + str(len(cities))
            for c in cities:
                univer_list = univer_list.union(set(University.objects.filter(city=c).distinct()))

            # map to dto
            univer_dto_list = []
            for u in univer_list:
                if len(salaries) > 0:
                    salary = salaries.get(str(u.city))
                u_dto = University.to_dto(u, '-')
                u_dto.salary = salary
                if len(crime_levels) > 0:
                    u_dto.crime_level = crime_levels[str(u.city)]
                else:
                    u_dto.crime_level = crime_level
                if u_dto.crime_level == crime_level:
                    univer_dto_list.append(u_dto)

            serializer = UniverSerializer()
            data = serializer.serialize(univer_dto_list)

            return HttpResponse(data, content_type='application/json')

        # filter by city
        if 'city' in body:
            if 'crime_level' in body:
                crime_level = body['crime_level']
                city_obj = City.objects.filter(name=body['city']).filter(city_crime_level=crime_level)
            else:
                city_obj = City.objects.filter(name=body['city'])
                crime_level = city_obj.first().city_crime_level

            # receive max salary in a particular city
            jobs = list(Job.objects.filter(city=city_obj).order_by('-salary'))
            if len(jobs) > 0:
                salary = str(jobs[0].salary)
            if 'salary' in body:
                jobs = list(
                    Job.objects.filter(city=city_obj).filter(salary__gte=int(body['salary'])).order_by('-salary'))
                if len(jobs) > 0:
                    salary = str(jobs[0].salary)
                    univer_list = univer_list.union(set(University.objects.filter(city=city_obj).distinct()))
            else:
                univer_list = univer_list.union(set(University.objects.filter(city=city_obj).distinct()))

            if city_obj == {} or city_obj.first() is None:
                pass
            else:
                region_obj = city_obj.first().region

        # filter by region
        if 'region' in body:
            region_ = Region.objects.filter(name=body['region'])
            if 'crime_level' in body:
                crime_level = body['crime_level']
            if crime_level == '' and region_.first() is not None:
                crime_level = region_.first().region_crime_level

            cities = City.objects.filter(region=region_)

            for c in cities:
                if 'salary' in body:
                    salary = body['salary']
                    jobs = list(Job.objects.filter(city=c).filter(salary__gte=int(body['salary'])))
                    if len(jobs) > 0:
                        univer_list = univer_list.union(set(University.objects.filter(city=c).distinct()))
                else:
                    univer_list = univer_list.union(set(University.objects.filter(city=c)))
                # receive max salary in a particular city
                jobs = list(Job.objects.filter(city=c).order_by('-salary'))
                if len(jobs) > 0:
                    salaries[str(c)] = str(jobs[0].salary)
                # receive crime level for a particular city
                crime_levels[str(c)] = str(c.city_crime_level)

            region_obj = region_.first()

            # filter by accommodations
        if 'accommodations' in body:
            temp_list = list(univer_list)
            for u in temp_list:
                accommodations = list(Accommodations.objects.filter(university=u))
                if len(accommodations) == 0:
                    temp_list.remove(u)
            univer_list = set(temp_list)

    else:
        univer_list = University.objects.all()

    # map to dto
    univer_dto_list = []
    for u in univer_list:
        if len(salaries) > 0:
            salary = salaries.get(str(u.city))
        u_dto = University.to_dto(u, region_obj.name)
        u_dto.salary = salary
        if len(crime_levels) > 0:
            u_dto.crime_level = crime_levels.get(str(u.city), City.objects.filter(name=u.city).first().city_crime_level)
        else:
            u_dto.crime_level = crime_level
        if u_dto.crime_level == crime_level:
            univer_dto_list.append(u_dto)

    serializer = UniverSerializer()
    data = serializer.serialize(univer_dto_list)

    print 'TEST:' + str(data)

    return HttpResponse(data, content_type='application/json')


@csrf_exempt
def all_cities(request):
    if request.method == 'POST':
        cities = City.objects.all()
        data = serializers.serialize('json', cities)

        return HttpResponse(data, content_type='application/json')


@csrf_exempt
def all_regions(request):
    if request.method == 'POST':
        regions = Region.objects.all()
        data = serializers.serialize('json', regions)

        return HttpResponse(data, content_type='application/json')


@csrf_exempt
def region_by_city(request):
    if request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)

    if 'city' in body:
        c = City.objects.filter(name=body['city'])
        region = Region.objects.filter(cities=c)

        if region is not None and region.first() is not None:
            data = serializers.serialize('json', region)
            return HttpResponse(data, content_type='application/json')


@csrf_exempt
def cities_by_region(request):
    if request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)

    if 'region' in body:
        r = Region.objects.filter(name=body['region'])
        cities = City.objects.filter(region=r)

        data = serializers.serialize('json', cities)
        return HttpResponse(data, content_type='application/json')


def populate_from_vk(request):
    from django.contrib.sites import requests
    req_cities = requests.get('https://api.vk.com/method/database.getCities?lang=ru&country_id=1&count=5')
    cities_json = req_cities.json()
    cities = cities_json['response']
    for city in cities:
        if "region" in city:
            region_name = city['region']
        else:
            region_name = city['title']
        region, created_region = Region.objects.update_or_create(name=region_name)
        city_obj, created_city = City.objects.update_or_create(name=city['title'], region=region)
        req_universities = requests.get(
            'https://api.vk.com/method/database.getUniversities?country_id=' + str(city['cid']) + '&city_id=' + str(
                city['cid']) + '&count=5')
        uni_json = req_universities.json()
        universities = uni_json['response']
        universities.pop(0)
        for university in universities:
            uni_obj = University(name=university['title'], city=city_obj, abbreviation='Y')
            uni_obj.save()
            req_faculties = requests.get(
                'https://api.vk.com/method/database.getFaculties?university_id=' + str(university['id']) + '&count=5')
            fac_json = req_faculties.json()
            faculties = fac_json['response']
            faculties.pop(0)
            for faculty in faculties:
                fac_obj = Department(name=faculty['title'], university=uni_obj)
                fac_obj.save()

   # logging.info('Successfully added regions, cities, universities and departments to the database!')

    return render_to_response('landing/index.html')


def fetch_specialties(request):
    thread = threading.Thread(target=universities.management.commands.fetch_specialties.fetch_specialties, args=())
    thread.start()
    result_string = 'Specialties, their descriptions and descriptions of possible positions for each specialty,' \
                    ' are being fetched and added to/updated in the database!'

    print result_string

    html = "<html><body>%s</body></html>" % result_string

    return HttpResponse(html)


def map_jobs_to_specialties(request):
    thread = threading.Thread(target=universities.management.commands.map_jobs_to_specialties.map_jobs_to_specialties(),
                              args=())
    thread.start()
    result_string = 'Jobs are being linked with the specialties in the database!'

    print result_string

    html = "<html><body>%s</body></html>" % result_string

    return HttpResponse(html)


def fetch_jobs_and_specialties_and_map_jobs_to_specialties(request):
    thread_fetch_cities = threading.Thread(target=universities.management.commands.populate_db.populate_from_vk,
                                           args=())
    thread_fetch_cities.start()
    result_string = 'Cities are being fetched and added to/updated in the database!'
    print result_string

    thread_fetch_specialties = threading.Thread(
        target=universities.management.commands.fetch_specialties.fetch_specialties, args=())
    thread_fetch_specialties.start()
    result_string = 'Specialties, their descriptions and descriptions of possible positions for each specialty,' \
                    ' are being fetched and added to/updated in the database!'
    print result_string

    thread_fetch_cities.join()
    result_string = 'Cities were successfully fetched and added to/updated in the database!'
    print result_string

    thread_fetch_jobs = threading.Thread(
        target=universities.management.commands.populate_db.get_specializations_and_vacancies(), args=())
    thread_fetch_jobs.start()
    result_string = 'Jobs, their categories, regions and salaries' \
                    ' are being fetched and added to/updated in the database!'
    print result_string

    thread_fetch_specialties.join()
    result_string = 'Specialties, their descriptions and descriptions of possible positions for each specialty,' \
                    ' were successfully fetched and added to the database!'
    print result_string
    thread_fetch_jobs.join()
    result_string = 'Jobs, their categories, regions and salaries' \
                    ' were successfully fetched and added to/updated in the database!'
    print result_string

    thread_map_jobs_to_specialties = threading.Thread(
        target=universities.management.commands.map_jobs_to_specialties.map_jobs_to_specialties(), args=())
    thread_map_jobs_to_specialties.start()
    result_string = 'Jobs are being linked with the specialties in the database!'
    print result_string

    thread_fetch_universities_first = threading.Thread(
        target=universities.management.commands.populate_db.parse_xml(), args=('data/universities1.xml'))
    thread_fetch_universities_first.start()
    result_string = 'Iniversities and specialities added from first xml'
    print result_string

    html = "<html><body>%s</body></html>" % result_string

    return HttpResponse(html)
