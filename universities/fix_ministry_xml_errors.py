pattern = 'xml version'
with open("unchanged.xml") as infile:
    found_patterns = 0
    with open('2.xml', 'w') as second:
        with open ('1.xml', 'w') as first:
            for line in infile:
                if found_patterns > 1:
                    second.write(line)
                else:
                    first.write(line)
                if pattern in line:
                    found_patterns += 1