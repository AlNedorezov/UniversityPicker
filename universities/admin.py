from django.contrib.admin import AdminSite
from django.contrib import admin
from django.http import HttpResponse

from .models import University, City, Region, Accommodations, Department
from .models import Specialty, Exam, UniversitiesSpecialtyExamMapping, JobCategory, Job, SpecialtyJob
from .models import EduLevel, EduType, FederalDistrict, UniversitiesSpecialty

import sys
reload(sys)
sys.setdefaultencoding('utf8')

admin.site.register(University)
admin.site.register(City)
admin.site.register(Region)
admin.site.register(Accommodations)
admin.site.register(Department)
admin.site.register(Specialty)
admin.site.register(Exam)
admin.site.register(UniversitiesSpecialtyExamMapping)
admin.site.register(JobCategory)
admin.site.register(Job)
admin.site.register(EduType)
admin.site.register(EduLevel)
admin.site.register(FederalDistrict)
admin.site.register(UniversitiesSpecialty)
admin.site.register(SpecialtyJob)


class MyAdminSite(AdminSite):
    def get_urls(self):
        from django.conf.urls import url
        urls = super(MyAdminSite, self).get_urls()
        urls += [
            url(r'^my_view/$', self.admin_view(self.my_view))
         ]
        return urls

    def my_view(self, request):
        return HttpResponse("Hey!")

admin_site = MyAdminSite()

admin_site.register(University)
admin_site.register(City)
admin_site.register(Region)
admin_site.register(Accommodations)
admin_site.register(Department)
admin_site.register(Specialty)
admin_site.register(Exam)
admin_site.register(UniversitiesSpecialtyExamMapping)
admin_site.register(JobCategory)
admin_site.register(Job)
admin_site.register(EduType)
admin_site.register(EduLevel)
admin_site.register(FederalDistrict)
admin_site.register(UniversitiesSpecialty)
admin_site.register(SpecialtyJob)

