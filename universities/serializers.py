# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.serializers.json import Serializer


class UniverSerializer(Serializer):
    def get_dump_object(self, obj):
        mapped_object = {
            'region': obj.region,
            'name': obj.name,
            'id': obj.id,
            'description': obj.description,
            'city': str(obj.city),
            'abbreviation': obj.abbreviation,
            'full_name': obj.full_name,
            'website': obj.website,
            'salary': str(obj.salary),
            'crime_level': obj.crime_level
        }

        return mapped_object


class UniverDetailSerializer(Serializer):
    def get_dump_object(self, obj):
        specialty_serializer = SpecialtySerializer()
        specialties = specialty_serializer.serialize(obj.specialties, ensure_ascii=False)

        mapped_object = {
            'region': obj.region,
            'name': obj.name,
            'id': obj.id,
            'description': obj.description,
            'city': str(obj.city),
            'abbreviation': obj.abbreviation,
            'full_name': obj.full_name,
            'website': obj.website,
            'salary': str(obj.salary),
            'crime_level': obj.crime_level,
            'specialties': specialties
        }

        return mapped_object


# todo for each...
class SpecialtySerializer(Serializer):
    def get_dump_object(self, obj):
        job_serializer = JobSerializer()
        jobs = job_serializer.serialize(obj.jobs, ensure_ascii=False)

        mapped_object = {'name': obj.name,
                         'description': obj.description,
                         'positions_description': obj.positions_description,
                         'jobs': jobs
                         }

        return mapped_object


class JobSerializer(Serializer):
    def get_dump_object(self, obj):
        mapped_object = {
            'name': str(obj.name),
            'city': str(obj.city),
            'salary': obj.salary
        }

        return mapped_object