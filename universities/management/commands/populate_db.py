# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from universities.models import *
import requests
import sys
import re
import json
import urllib
from lxml import etree

reload(sys)
#sys.setdefaultencoding('utf8')

def get_city_from_adress(adress):
    # print adress
    if not adress or adress == "":
        return ""
    city_name = ""
    for token in adress.split(','):
        candidate = token.lstrip(' ')
        if candidate.split(' ')[0] == "город":
            city_name = candidate[5:].lstrip(' ')
        if candidate.split(' ')[0] == "г.":
            city_name = candidate[2:].lstrip(' ')
        if candidate.split(' ')[0] == "с.":
            city_name = candidate[2:].lstrip(' ')
        if candidate.split(' ')[0] == "пос.":
            city_name = candidate[4:].lstrip(' ')
        if candidate.split('.')[0] == "г":
            city_name = candidate[2:].lstrip(' ')
    # if city_name == "":
    #    print adress
    return city_name

def find(node, tag):
    result = ""
    if node.find(tag) is not None and node.find(tag).text is not None:  # and text is not a bullshit?
        result = node.find(tag).text
    return result


def get_universities_from_xml(xml_file_name):
    tree = etree.parse(xml_file_name)
    root = tree.getroot()
    i = 0
    for university in root[0]:
        i += 1
        print i

        adress = find(university, 'PostAddress')
        city_name = get_city_from_adress(adress)
        full_name = find(university, 'EduOrgFullName')
        name = find(university, 'EduOrgShortName')
        abbreviation = find(university, 'FederalDistrictShortName')
        # description = university.find('')
        is_federal = find(university, 'IsFederal') == "1"
        # license_issue_date = university.find('')
        # license_due_date = university.find('')
        post_address = adress
        is_branch = find(university, 'IsBranch') == "1"
        rector_name = find(university, 'HeadName')
        form_name = find(university, 'FormName')
        type_name = find(university, 'TypeName')
        website = find(university.find('ActualEducationOrganization'), 'WebSite')
        phone = find(university.find('ActualEducationOrganization'), 'Phone')
        fax = find(university.find('ActualEducationOrganization'), 'Fax')
        email = find(university.find('ActualEducationOrganization'), 'Email')

        city_objects = City.objects.filter(name=city_name)
        if city_objects.count() == 0:
            continue

        city_obj = city_objects[0]
        uni, created = University.objects.get_or_create( \
            city=city_obj, \
            full_name=full_name, \
            name=name, \
            abbreviation=abbreviation, \
            # description = models.TextField(default='')
            is_federal=is_federal, \
            # license_issue_date = models.DateField(default=timezone.now)
            # license_due_date = models.DateField(default=timezone.now)
            post_address=post_address, \
            is_branch=is_branch, \
            rector_name=rector_name, \
            form_name=form_name, \
            type_name=type_name, \
            website=website, \
            phone=phone, \
            fax=fax, \
            email=email)
        print 'ok'

        if university.find('Supplements') is not None:
            for suplement in university.find('Supplements'):
                if suplement.find('EducationalPrograms') is not None:
                    for program in suplement.find('EducationalPrograms'):
                        spec_name = find(program, 'UGSName')
                        spec_ugs_code = find(program, 'UGSCode')
                        spec_qualification = find(program, 'Qualification')
                        spec_is_accredited = find(program, 'IsAccredited') == "1"
                        spec_is_cancelled = find(program, 'IsCanceled') == "1"
                        spec_is_suspended = find(program, 'IsSuspended') == "1"

                        spec_edu_level_code = find(program, 'EduLevelCode')
                        spec_edu_level_name = find(program, 'EduLevelName')
                        spec_edu_level, created = EduLevel.objects.get_or_create( \
                            name=spec_edu_level_name, \
                            code=spec_edu_level_code)

                        spec_edu_type_name = find(program, 'TypeName')
                        spec_edu_type, created = EduType.objects.get_or_create( \
                            name=spec_edu_type_name)

                        spec = Specialty.objects.filter(ugs_code=spec_ugs_code)
                        spec.update(qualification=spec_qualification)
                        spec.update(is_accredited=spec_is_accredited)
                        spec.update(is_cancelled=spec_is_cancelled)
                        spec.update(is_suspended=spec_is_suspended)
                        spec.update(edu_level=spec_edu_level)
                        spec.update(edu_type=spec_edu_type)
                        if spec.count() > 0:
                            UniversitiesSpecialty.objects.get_or_create(university=uni, specialty=spec[0])
                            print "speciality added"


# todo remove all comments in the end of the file
def parse_xml(xml_file_name):
    get_regions_from_xml(xml_file_name)
    get_universities_from_xml(xml_file_name)


def get_regions_from_xml(xml_file_name):
    tree = etree.parse(xml_file_name)
    root = tree.getroot()
    i = 0

    for university in root[0]:
        i += 1
        if i % 100 == 0:
            print i

        region_name = find(university, 'RegionName')
        region_code = find(university, 'RegionCode')
        fd_name = find(university, 'FederalDistrictName')
        fd_code = find(university, 'FederalDistrictCode')

        fd, created = FederalDistrict.objects.get_or_create(name=fd_name, code=fd_code)

        region = Region.objects.filter(name=region_name)
        region.update(code=region_code)
        region.update(federal_district=fd)

        # todo: doesn't work for republics. Fix it later.
        # for region_short_name in region_name.split(' '):
        #    region = Region.objects.filter(name=region_short_name)
        #    region.update(code=region_code)
        #    region.update(federal_district=fd)


def populate_from_vk():
    get_cities('https://api.vk.com/method/database.getCities?lang=ru&country_id=1&need_all=1&count=1000')
    get_cities( 'https://api.vk.com/method/database.getCities?lang=ru&country_id=1')

    print 'Successfully added regions and cities'


def get_cities(url):
    req_cities = requests.get(url)
    if req_cities.status_code != 200:
        print "Can't get regions and cities"
        return

    cities_json = req_cities.json()
    cities = cities_json['response']
    for city in cities:
        if not any(char.isdigit() for char in city['title']):
            if "region" in city:
                region_name = city['region']
            else:
                region_name = city['title']
            region, created_region = Region.objects.update_or_create(name=region_name)
            city_obj, created_city = City.objects.update_or_create(name=city['title'], region=region)

            '''
            req_universities = requests.get('https://api.vk.com/method/database.getUniversities?country_id=' + str(
                city['cid']) + '&city_id=1&count=10')
            uni_json = req_universities.json()
            universities = uni_json['response']
            universities.pop(0)
            for university in universities:
                uni_obj = University(name=university['title'], city=city_obj, abbreviation='Y')
                uni_obj.save()
                req_faculties = requests.get('https://api.vk.com/method/database.getFaculties?university_id=' + str(
                    university['id']) + '&count=10')
                fac_json = req_faculties.json()
                faculties = fac_json['response']
                faculties.pop(0)
                for faculty in faculties:
                    fac_obj = Department(name=faculty['title'], university=uni_obj)
                    fac_obj.save()
            '''

def populate_region_crime_level(self):
    url = "https://rospravosudie.com/research/crime_map_population.html"
    pat = re.compile('<script type="text/javascript">([\s\S]*?)</script>')

    sock = urllib.urlopen(url).read()
    scripts = pat.findall(sock)

    region_pattern = re.compile('mapInfoContent_regionId2name = {(.+?)}')
    region_names = region_pattern.findall(scripts[0])

    region_fix_json = '{' + region_names[0] + '}'
    region_fixed_json = re.sub(r"([0-9]+):", r"'\1':", region_fix_json)
    region_fixed_json = region_fixed_json.replace("'", '"')
    region_json = json.loads(region_fixed_json)
    counter = 1
    crime_regions = {}
    max = 0
    min = sys.maxint
    while counter <= len(region_json):
        if counter < 10:
            key = '0' + str(counter)
        else:
            key = str(counter)
        # this is not the best way but programmers have a life too
        # it adds a 1 to the sum with is not part of the crime levels
        # but since it adds it to every region it is not a problem
        data_pattern = re.compile("'" + key + "':([0-9]+)")
        data = data_pattern.findall(scripts[0])
        sum = 0
        for d in data:
            sum += int(d)
        if (sum > 0):
            if (max < sum):
                max = sum
            if (min > sum):
                min = sum
            crime_regions[region_json[str(counter)]] = sum
        counter += 1

    range = (max - min) / 3
    for c in crime_regions:
        if (crime_regions[c] < min + range):
            crime_level = 'low'
        elif (crime_regions[c] < min + 2 * range):
            crime_level = 'medium'
        else:
            crime_level = 'high'
        Region.objects.filter(name=c).update(region_crime_level=crime_level)
    print 'Successfully populated the crime level in regions'

def get_specializations_and_vacancies():
    response = requests.get('https://api.hh.ru/specializations')
    if response.status_code != 200:
        print "can't get specialisations"
        return
    specialisations_json = response.json()

    for bunch in specialisations_json:
        category_name = bunch['name']
        category_id = bunch['id']
        specialisation_obj, created = JobCategory.objects.get_or_create(name=category_name)
        per_page = 500
        response = requests.get(
            'https://api.hh.ru/vacancies/?per_page=' + str(per_page) + '&locale=RU&specialization=' + category_id)
        if response.status_code != 200:
            print "can't get vacancies"
            return

        vacancies_json = response.json()
        vacancies = vacancies_json['items']
        print 'number of vacancies = ', len(vacancies)
        for vacancy in vacancies:

            # if used fields exists
            if 'salary' in vacancy and vacancy['salary'] and 'from' in vacancy[
                'salary'] and vacancy['salary']['from'] and 'name' in vacancy and 'address' in vacancy and vacancy[
                'address'] and 'city' in \
                    vacancy['address']:
                # print vacancy['salary']['from']
                # print vacancy['name']
                # print vacancy['address']['city']
                # print '----------'

                city_objects = City.objects.filter(name=vacancy['address']['city'])
                if city_objects.count() > 0:
                    city_obj = city_objects[0]
                    job, created = Job.objects.get_or_create(name=vacancy['name'], salary=vacancy['salary']['from'],
                                                             job_category=specialisation_obj, city=city_obj)
                #else:
                    #print 'city ', vacancy['address']['city'], ' is not in db'

    print 'vacancies sucessfully added'


class Command(BaseCommand):
    args = '<>'
    help = 'Command for populating the database with information from different sources like VK...'

    def hasNumbers(inputString):
        return any(char.isdigit() for char in inputString)



    def handle(self, *args, **options):
        populate_from_vk()
        self._populate_accommodations()
        populate_region_crime_level()
        get_specializations_and_vacancies()
        parse_xml('data/small.xml')
        parse_xml('data/universities1.xml')
        parse_xml('data/universities2.xml')

