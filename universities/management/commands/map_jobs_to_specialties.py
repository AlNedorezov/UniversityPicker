# !/usr/bin/env python
# coding: UTF-8

from django.core.management.base import BaseCommand
from universities.models import Specialty, Job, SpecialtyJob
from nltk.stem import SnowballStemmer


def map_jobs_to_specialties():

    stemmer = SnowballStemmer("russian")

    specialties = Specialty.objects.all()
    jobs = Job.objects.all()

    skipped_words = ['в', 'и', 'по', 'с', 'на', '–', 'за', 'для', 'работ', 'специалист']

    # https://stackoverflow.com/questions/6181763/converting-a-string-to-a-list-of-words

    for i, specialty in enumerate(specialties):
        specialty_description = str(specialties[i].description)
        specialty_possible_positions_prose = str(specialties[i].possible_positions_prose)
        if specialty_possible_positions_prose == '':
            specialty_prose = specialty_possible_positions_prose + ' ' + specialty_description
        else:
            specialty_prose = specialty_possible_positions_prose
        specialty_prose_word_list = specialty_prose.split(' ')
        for word in specialty_prose_word_list:
            word_without_symbols = word.replace("/ul", "")
            word_without_symbols = word_without_symbols.replace("/li", "")
            word_without_symbols = word_without_symbols.replace("ul", "")
            word_without_symbols = word_without_symbols.replace("li", "")
            word_without_symbols = word_without_symbols.translate(None, '.!<>,/-):')
            word_without_symbols = word_without_symbols.replace(" ", "")
            word_without_symbols = word_without_symbols.decode('utf-8').lower()
            word_without_symbols = stemmer.stem(word_without_symbols)
            if word_without_symbols != '':
                for j, job in enumerate(jobs):
                    job_name = str(jobs[j].name)
                    job_name_word_list = job_name.split(' ')
                    for word_from_job_name in job_name_word_list:
                        word_from_job_name_without_symbols = \
                            word_from_job_name.translate(None, '.!<>,/-):')
                        word_from_job_name_without_symbols = word_from_job_name_without_symbols.replace(" ", "")
                        word_from_job_name_without_symbols = word_from_job_name_without_symbols.decode('utf-8').lower()
                        word_from_job_name_without_symbols = stemmer.stem(word_from_job_name_without_symbols)
                        if word_from_job_name_without_symbols != '':
                            if word_without_symbols == word_from_job_name_without_symbols and word_without_symbols not in skipped_words:
                                SpecialtyJob.objects.update_or_create(specialty=specialties[i], job=jobs[j])
                                # print specialties[i].name + "   " + jobs[j].name + "   '" + word_without_symbols + "'   '" + word_from_job_name_without_symbols + "'"
        print 'Jobs were successfully linked with the specialty ' + specialties[i].name

    print 'Jobs were successfully linked with all specialties in the database'


class Command(BaseCommand):

    def handle(self, *args, **options):
        map_jobs_to_specialties()
