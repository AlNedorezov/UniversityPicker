# !/usr/bin/env python
# coding: UTF-8

from django.core.management.base import BaseCommand
from grab import Grab
from universities.models import Specialty


def fetch_specialties():

    specialties = []

    g = Grab()
    g.go('http://vuz.edunetwork.ru/specs/#3')
    if g.response.code == 200:
        for i, s in enumerate(g.xpath_list('//*[@href]')):
            if 13 < i < 459:  # work only with blocks with data about specialties
                url = 'http://vuz.edunetwork.ru' + s.get('href')
                params = [s.text]
                if url[24:31] == '/specs/':
                    g.go(url)
                    if g.response.code == 200:
                        params.append(g.xpath_list('//*[@content]')[0].get('content'))
                        params.append(g.xpath_list('//article/p[2]')[0].text)
                specialties.append(params)

    g.go('http://vuz.edunetwork.ru/specs/#3')
    if g.response.code == 200:
        for i, s in enumerate(g.xpath_list('//ul/li/span')):
            if i != 0:
                specialties[i-1].append(s.text)

    for specialty in specialties:
        if len(specialty) is 4:
            if specialty[1] is None:
                description = ''
            else:
                description = specialty[1]
            if specialty[2] is None:
                possible_positions_prose = ''
            else:
                possible_positions_prose = specialty[2]
            Specialty.objects.update_or_create(name=specialty[0], description=description,
                                               possible_positions_prose=possible_positions_prose,
                                               ugs_code=specialty[3])


class Command(BaseCommand):

    def handle(self, *args, **options):
        fetch_specialties()
