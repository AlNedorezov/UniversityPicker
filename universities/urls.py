from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<university_id>\w+\-\w.+)/$', views.university, name='universities'),
    url(r'^list/', views.universities_list, name='universities_list'),
    url(r'^details/', views.university_details, name='university_details'),
    url(r'^index/', views.index, name='index'),
    url(r'^detail/', views.detail, name='detail'),
    url(r'^vk/', views.populate_from_vk, name='vk'),
    url(r'^fetchspecialties/', views.fetch_specialties, name='fetch_specialties'),
    url(r'^mapjobstospecialties/', views.map_jobs_to_specialties, name='map_jobs_to_specialties'),
    url(r'^fetchandmapjobstospecialties/', views.fetch_jobs_and_specialties_and_map_jobs_to_specialties,
        name='fetch_jobs_and_specialties_and_map_jobs_to_specialties'),
    url(r'^vk/', views.populate_from_vk, name='vk'),
    url(r'^allcities/', views.all_cities, name='allcities'),
    url(r'^allregions/', views.all_regions, name='allregions'),
    url(r'^regionbycity/', views.region_by_city, name='regionbycity'),
    url(r'^citiesbyregion/', views.cities_by_region, name='citiesbyregion')
]
