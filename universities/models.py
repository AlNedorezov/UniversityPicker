from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
import uuid, logging


class FederalDistrict(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200, default='name')
    code = models.CharField(max_length=50, default='-')

    def __str__(self):
        return self.name


class Region(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=300, default='name')
    region_crime_level = models.CharField(max_length=300, default='crime level')
    code = models.IntegerField(default=0)
    federal_district = models.ForeignKey(FederalDistrict, on_delete=models.CASCADE,
                                         related_name='regions', null=True)  # todo get rid of nullable values

    def __str__(self):
        return self.name


class City(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=300, default='name')
    city_crime_level = models.CharField(max_length=300, default='crime level') 
    region = models.ForeignKey(Region, on_delete=models.CASCADE,
                               related_name='cities', null=True)  # todo get rid of nullable values
     
    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Cities'


class University(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    city = models.ForeignKey(City, on_delete=models.CASCADE,
                             related_name='universities', null=True)  # todo get rid of nullable values
    full_name = models.CharField(max_length=500, default='full name')
    name = models.CharField(max_length=500, default='name')
    abbreviation = models.CharField(max_length=50, default='-')
    description = models.TextField(default='')
    is_federal = models.BooleanField(default=False)
    license_issue_date = models.DateField(default=timezone.now)
    license_due_date = models.DateField(default=timezone.now)
    post_address = models.CharField(max_length=500, default='-')
    is_branch = models.BooleanField(default=False)
    rector_name = models.CharField(max_length=150, default='-')
    form_name = models.CharField(max_length=100, default='-')
    type_name = models.CharField(max_length=200, default='-')
    website = models.CharField(max_length=500, default='website')
    phone = models.CharField(max_length=100, default='-')
    fax = models.CharField(max_length=100, default='-')
    email = models.CharField(max_length=150, default='-')

    def __str__(self):
        return self.abbreviation + ' ' + self.name

    def _abbreviation_(self):
        return self.abbreviation

    def _name_(self):
        return self.name

    def to_dto(university, region):
        if university is None: return None
#        dto = DTO()
        dto = University()
        dto.region = region
#        dto.salary = salary
        dto.id = university.id
        dto.abbreviation = university.abbreviation
        dto.name = university.name
        dto.full_name = university.full_name
        dto.description = university.description
        dto.city = university.city
        dto.email = university.email
        dto.fax = university.fax
        dto.phone = university.phone
        dto.website = university.website
        dto.type_name = university.type_name
        dto.form_name = university.form_name
        dto.rector_name = university.rector_name
        dto.is_branch = university.is_branch
        dto.post_address = university.post_address
        dto.license_due_date = university.license_due_date
        dto.is_federal = university.is_federal
        print('city:' + str(dto))
        return dto

    def get_all(self):
        universities = University.all().fetch(1000)
        universities_dto = []
        for u in universities:
            universities_dto.append(self.to_dto(u))
        return universities_dto

    class Meta:
        verbose_name_plural = 'Universities' 


class Department(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    university = models.ForeignKey(University, on_delete=models.CASCADE,
                                   related_name='departments', null=True)  # todo get rid of nullable values
    name = models.CharField(max_length=300, default='name')
    description = models.TextField(default='')
    phone = models.CharField(max_length=100, default='-')
    email = models.CharField(max_length=150, default='-')
    
    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Departments'


class EduLevel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100, default='name')
    code = models.CharField(max_length=50, default='-')

    def __str__(self):
        return self.name


class EduType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200, default='name')

    def __str__(self):
        return self.name


class Specialty(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=300, default='name')
    ugs_code = models.CharField(max_length=8, default='-')
    qualification = models.CharField(max_length=500, default='-')
    description = models.TextField(default='')
    possible_positions_prose = models.TextField(default='')
    is_accredited = models.BooleanField(default=False)
    is_cancelled = models.BooleanField(default=False)
    is_suspended = models.BooleanField(default=False)
    edu_level = models.ForeignKey(EduLevel, on_delete=models.CASCADE,
                                  related_name='specialties', null=True)  # todo get rid of nullable values
    edu_type = models.ForeignKey(EduType, on_delete=models.CASCADE,
                                 related_name='specialties', null=True)  # todo get rid of nullable values

    def __str__(self):
        return self.name



    class Meta:
        verbose_name_plural = 'Specialties'


class UniversitiesSpecialty(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    university = models.ForeignKey(University, on_delete=models.CASCADE,
                                   related_name='university_specialties', null=True)  # todo get rid of nullable values
    specialty = models.ForeignKey(Specialty, on_delete=models.CASCADE,
                                  related_name='university_specialties', null=True)  # todo get rid of nullable values
    department = models.ForeignKey(Department, on_delete=models.CASCADE,
                                   related_name='university_specialties', null=True)  # todo get rid of nullable values

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name_plural = 'University Specialties'


class Exam(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=300, default='name')
    year = models.IntegerField(default=2016)
    minimal_passing_grade = models.IntegerField(default=100)
    
    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name_plural = 'Exams'


class UniversitiesSpecialtyExamMapping(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    universities_speciality = models.ForeignKey(UniversitiesSpecialty,
                                                on_delete=models.CASCADE, null=True)  # todo get rid of nullable values
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE, null=True)  # todo get rid of nullable values
    grade = models.IntegerField(default=100)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name_plural = 'UniversitiesSpecialtyExamMappings'


class Accommodations(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    city = models.ForeignKey(City, on_delete=models.CASCADE,
                                   related_name='accommodations', null=True)  # todo get rid of nullable values.
    name = models.CharField(max_length=300, default='accommodation')
    cost = models.DecimalField(max_digits=7, decimal_places=2)
    
    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Accommodations'


class JobCategory(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=300, default='name')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'JobCategories'


class Job(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=300, default='name')
    salary = models.IntegerField(default=5000)
    job_category = models.ForeignKey(JobCategory, on_delete=models.CASCADE,
                                     related_name='jobs', null=True)  # todo get rid of nullable values
    city = models.ForeignKey(City, on_delete=models.CASCADE,
                             related_name='jobs', null=True)  # todo get rid of nullable values

    def __str__(self):
        return self.name


class SpecialtyJob(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    specialty = models.ForeignKey(Specialty, on_delete=models.CASCADE,
                                  related_name='specialty_jobs', null=True)  # todo get rid of nullable values
    job = models.ForeignKey(Job, on_delete=models.CASCADE,
                            related_name='specialty_jobs', null=True)  # todo get rid of nullable values

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name_plural = 'Specialty Jobs'

class DTO(object):
    pass

class SpecialtyDTO(Specialty):
    jobs = []

    def to_dto(specialty):
        if specialty is None: return None
        dto = SpecialtyDTO()
        dto.id = specialty.id
        dto.name = str(specialty.name)
        dto.description = specialty.description
        dto.positions_description = specialty.positions_description
        dto.jobs = specialty.jobs
        print('specialty:' + str(dto))

        return dto