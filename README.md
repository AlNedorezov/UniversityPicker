# University Picker

## Project Description
The purpose of the application is to help high school graduates to choose the university and the specialization based on the Russian State Exams results, the cost of the education, scholarships, the location of the university, the standard of living, average salary sizes, the number of preferred job offers, the cost of rental housing and hostels in that location.

## Installation notes
* [PostgreSQL](https://www.postgresql.org) should be installed on you machine
* Create new database with PostgreSQL
* Enter you database name, owner (PostgreSQL user) and his password, PostgreSQL host and port in [settings.py](https://gitlab.com/AlNedorezov/UniversityPicker/blob/develop/UniversityPicker/settings.py) in corresponding fields after the line `DATABASES = {`
* Before running the project execute the following lines of code:
  1. `pip install -r requirements.txt` in order to install the packages required by the project
  2. `python manage.py makemigrations` to create new migrations according to the model
  3. `python manage.py migrate --fake-initial` to apply created migrations
  4. `python manage.py migrate --run-syncdb` to create tables for the applications
* Then you can run `python manage.py runserver` to start the server

More details can be found in the [project wiki](https://gitlab.com/AlNedorezov/UniversityPicker/wikis/home).